import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Header from './Header';
import { counterIncrement, counterDecrement } from '../store/action/counterAction'
import { fetchEmplist } from '../store/action/getEmpListAction'
import {tablealign} from './header.css'
class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }
  componentDidMount() {
    this.props.fetchEmplist()
  }
  render() {
    let { counter } = this.props
    console.log(this.props);
    return (
      <div>
        <Header />
        <h1>
          List of Employees
        </h1>
        <div className={tablealign}>
          <table>
            <thead>
              <tr>
                <th>FirstName</th>
                <th>LastName</th>
                <th>Email</th>
                <th>Qulification</th>
                <th>Profession</th>
                <th>Location</th>
                <th>Mobile</th>
                <th>Gender</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {
                this.props.empListData.empList && this.props.empListData.empList.length>0 &&
                this.props.empListData.empList.map((item,index) => {
                  let {firstname,lastname,email,gender,location,qualiication,proession,Phone} = item
                  return(
                  <tr key={index}>
                    <td>{firstname}</td>
                    <td>{lastname}</td>
                    <td>{email}</td>
                    <td>{qualiication}</td>
                    <td>{proession}</td>
                    <td>{location}</td>
                    <td>{Phone}</td>
                    <td>{gender}</td>
                    <td>Edit/Delete</td>
                  </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    counter: state.counter.counter,
    empListData: state.empList
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ counterIncrement, counterDecrement, fetchEmplist }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Home)