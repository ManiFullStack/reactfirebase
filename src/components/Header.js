import React, { Component } from 'react'
import { headerbg, headermenu, appmenu } from './header.css'
import { Link } from 'react-router-dom'
class Header extends Component {
    render() {
        return (
            <div>
                <div className={headerbg}>
                    <h2 className={appmenu}>Employee List</h2>
                    <div className={headermenu}>
                        <p >
                            <Link to="/">
                                Home
                            </Link>
                        </p>
                        <p>
                            <Link to="/addemp" >
                                AddEmp
                            </Link>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}
export default Header