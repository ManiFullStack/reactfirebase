import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Header from './Header';
import {AddNewEmp} from '../store/action/getEmpListAction'
import { addempstyle, parAddEmp, addEmpForm, addEmpbtn } from './header.css'
class AddEmp extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }
  componentDidMount() {
  }
  handleInput = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }
  addEmpForm=(e)=>{
    e.preventDefault()
    this.props.AddNewEmp(this.state,this.props.history)
  }
  render() {
    return (
      <div>
        <Header />
        <h1>
          Add Employee
        </h1>
        <div className={parAddEmp}>
          <div className={addempstyle}>
            <form onSubmit={this.addEmpForm}>
              <div className={addEmpForm}>
                <label>FirstName</label>
                <input onChange={this.handleInput} name="firstname" type="text" />
              </div>
              <div className={addEmpForm}>
                <label>LastName</label>
                <input onChange={this.handleInput} name="lastname" type="text" />
              </div>
              <div className={addEmpForm}>
                <label>Email</label>
                <input onChange={this.handleInput} name="email" type="email" />
              </div>
              <div className={addEmpForm}>
                <label>Qualification</label>
                <input onChange={this.handleInput} name="qualiication" type="text" />
              </div>
              <div className={addEmpForm}>
                <label>Profession</label>
                <input onChange={this.handleInput} name="proession" type="text" />
              </div>
              <div className={addEmpForm}>
                <label>Location</label>
                <input onChange={this.handleInput} name="location" type="text" />
              </div>
              <div className={addEmpForm}>
                <label>Mobile</label>
                <input onChange={this.handleInput} name="Phone" type="text" />
              </div>
              <div className={addEmpForm}>
                <label>Gender</label>
                <input onChange={this.handleInput} name="gender" type="text" />
              </div>
              <div className={addEmpbtn}>
                <button>Add Employee</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
const mapDispatchToProps=(dispatch)=>{
  return bindActionCreators({AddNewEmp},dispatch)
}

export default connect(null,mapDispatchToProps)(AddEmp)