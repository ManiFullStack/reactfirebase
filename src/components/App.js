import React,{lazy,Suspense} from 'react';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';
import importedComponent from 'react-imported-component';
import Home from './Home';
import Loading from './Loading';
const AddEmp = lazy(()=>import(/* webpackChunkName: "AddEmp" */ "./AddEmp"))
// const AsyncDynamicPAge = importedComponent(
//   () => import(/* webpackChunkName:'DynamicPage' */ './DynamicPage'),
//   {
//     LoadingComponent: Loading
//   }
// );


const App = () => {
  return (
    <Suspense fallback="<div>Loading ....</div>">
    <Router>
      <div>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/addemp" component={AddEmp}/>
        </Switch>
      </div>
    </Router>
    </Suspense>
  );
};

export default App;
