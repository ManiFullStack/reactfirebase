import { Decrement, Increment } from "../../constant"

export const counterIncrement =()=>{
    return{
        type: Increment,
        payload:2
    }
}
export const counterDecrement =()=>{
    return{
        type: Decrement,
        payload:2
    }
}