import Axios from 'axios'
import {GetFetechedData, PostData} from '../../constant'
export const fetchEmplist=() => {
    let data=[]
    return(dispatch)=>{
        Axios.get("https://reactdatabase-ee84a-default-rtdb.firebaseio.com/empList.json")
        .then(res=>{
            for(let item in res.data){
                data.push({...res.data[item],id:item})
            }
            dispatch(fetchedEmpList(data))
        })
    }
}

export const fetchedEmpList =(payload) => {
    return{
        type:GetFetechedData,
        payload
    }
}
export const AddNewEmp =(payload,history) => {
    return(dispatch=>{
        Axios.post(`https://reactdatabase-ee84a-default-rtdb.firebaseio.com/empList.json`,payload)
        .then(res=>{
            dispatch(newlyAddedEmp(payload))
            history.push("/")
        })
    })
}

export const newlyAddedEmp =(payload)=>{
    return{
        type:PostData,
        payload
    }
}