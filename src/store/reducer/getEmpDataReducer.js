import { GetFetechedData, PostData } from "../../constant"

const initialState={
    empList:[
    ]
}

const emplistReducer=(state=initialState,action) => {
    switch (action.type){
        case GetFetechedData:
            return{
                ...state,
                empList:action.payload
            }
        case PostData:
            let data = [...state.empList]
            data.push(action.payload)
            return{
                ...state,
                empList:data
            }
        default:
            return state
    }
}

export default emplistReducer