import { Decrement, Increment } from "../../constant";

const initialState = {
    counter:1
}

const counterReducer = (state=initialState,action)=>{
    switch (action.type) {
        case Increment:
            return{
                ...state,
                counter:state.counter+action.payload
            }
        case Decrement:
            return{
                ...state,
                counter:state.counter-action.payload
            }
    
        default:
            return state
    }
}

export default counterReducer