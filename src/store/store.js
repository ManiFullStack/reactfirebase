import { createStore,combineReducers,applyMiddleware } from 'redux'
import counterReducer from './reducer/counterReducer'
import thunk from 'redux-thunk'
import emplistReducer from './reducer/getEmpDataReducer'
const rootReducer = combineReducers({
    counter:counterReducer,
    empList:emplistReducer
})
const store = createStore(rootReducer,applyMiddleware(thunk))


export default store